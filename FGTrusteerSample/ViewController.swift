//
//  ViewController.swift
//  FGTrusteerSample
//
//  Created by Lahiru Chathuranga on 2023-09-07.
//

import UIKit
import FGTrusteer

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        validateDevice()
    }
    
    func validateDevice() {
        let result = FGTrusteerSDK.getInstance().validateDevice()
        print(result.getData()?.message)
    }
}

