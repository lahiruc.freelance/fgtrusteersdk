//
//  DeviceHelperProtocol.swift
//  FGTrusteer
//
//  Created by Lahiru Chathuranga on 2023-09-06.
//

import Foundation
import UIKit

public protocol FGTDeviceSecControllerProtocol {
    func getCurrentDevice() -> CurrentDevice
    func jailBroken() -> Bool
    func simulator() -> Bool
    func invalidSource() -> Bool
    func debugged() -> Bool
    func isSecuredWIFIConnection() -> Bool
}
