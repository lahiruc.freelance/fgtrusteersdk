//
//  FGTrusteer.swift
//  FGTrusteer
//
//  Created by Lahiru Chathuranga on 2023-09-07.
//

import Foundation

public class FGTrusteerSDK {
    public var security: FGTDeviceSecControllerProtocol?
    public class func getInstance() -> FGTrusteerSDK { FGTrusteerSDK() }
    
    private init(security: FGTDeviceSecControllerProtocol? = FGTDeviceSecController.default) {
        self.security = security
    }
    
    public func validateDevice() -> FGTResult<FGTMessage> {
        if security?.simulator() ?? false {
            return FGTResult.init(false, FGTMessages.ERROR_MESSAGE_SIMULATOR.get())
        } else if security?.jailBroken() ?? false {
            return FGTResult.init(false, FGTMessages.ERROR_MESSAGE_ROOTED.get())
        } else if security?.debugged() ?? false {
            return FGTResult.init(false, FGTMessages.ERROR_MESSAGE_DEBUG.get())
        } else if security?.invalidSource() ?? false {
            return FGTResult.init(false, FGTMessages.ERROR_MESSAGE_INVALID_SOURCE.get())
        } else if security?.isSecuredWIFIConnection() ?? false {
            return FGTResult.init(false, FGTMessages.ERROR_MESSAGE_UNENCRYPTED_WIFI.get())
        }
        
        return FGTResult.init(true, nil)
    }
}
