//
//  FGTResult.swift
//  FGTrusteer
//
//  Created by Lahiru Chathuranga on 2023-09-07.
//

import Foundation

public class FGTResult<T> {
    /** Result is valid or not*/
    public var state: Bool = false
    
    /** Result data to be passed in and out*/
    private var data: T? = nil
    
    func setData(_ data: T) {
        self.data = data
    }
    
    public func getData() -> T? {
        return data
    }
    
    /** Error message if the status is false*/
    public var error: String? = nil
    
    public var errorCode: String? = nil
    
    public var errorDescription: String? = nil
    
    private func defaultInit() {
        state = false
        // MARK: - NOTE - Default error should be here
    }
    
    init() {
        defaultInit()
    }
    
    init(_ state: Bool, _ data: T?) {
        self.defaultInit()
        self.state = state
        self.data = data
    }
}
