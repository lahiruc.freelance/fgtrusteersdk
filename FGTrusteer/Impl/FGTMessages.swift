//
//  FGTMessages.swift
//  FGTrusteer
//
//  Created by Lahiru Chathuranga on 2023-09-07.
//

import Foundation

enum FGTMessages {
    
    case ERROR_MESSAGE_SIMULATOR
    case ERROR_MESSAGE_ROOTED
    case ERROR_MESSAGE_DEBUG
    case ERROR_MESSAGE_INVALID_SOURCE
    case ERROR_MESSAGE_UNENCRYPTED_WIFI
    
    func get() -> FGTMessage {
        switch self {
        case .ERROR_MESSAGE_SIMULATOR:
            return FGTMessage(
                "Oops!",
                "Simulators are not allowed to login.",
                "0x100",
                "OK",
                "Cancel"
            )
        case .ERROR_MESSAGE_ROOTED:
            return FGTMessage(
                "Oops!",
                "Jail broken devices are not allowed to login.",
                "0x101",
                "OK",
                "Cancel"
            )
        case .ERROR_MESSAGE_DEBUG:
            return FGTMessage(
                "Oops!",
                "App is not allowed to debug.",
                "0x102",
                "OK",
                "Cancel"
            )
        case .ERROR_MESSAGE_INVALID_SOURCE:
            return FGTMessage(
                "Oops!",
                "App is not downloaded from the App Store.",
                "0x103",
                "OK",
                "Cancel"
            )
        case .ERROR_MESSAGE_UNENCRYPTED_WIFI:
            return FGTMessage(
                "Oops!",
                "We are terminating the app because app cannot be run on unsecure networks.",
                "0x104",
                "OK",
                "Cancel"
            )
        }
    }
}

public struct FGTMessage {
    
    public var title: String
    public var message: String
    public var errorCode: String
    public var buttonP: String
    public var buttonN: String
    
    init(_ title: String, _ message: String, _ errorCode: String, _ buttonP: String, _ buttonN: String) {
        self.title = title
        self.message = message
        self.errorCode = errorCode
        self.buttonP = buttonP
        self.buttonN = buttonN
    }
}
