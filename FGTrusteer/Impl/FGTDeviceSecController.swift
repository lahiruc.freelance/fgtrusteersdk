//
//  FGTDeviceSecController.swift
//  FGTrusteer
//
//  Created by Lahiru Chathuranga on 2023-09-07.
//

import Foundation
import UIKit
import DeviceCheck
import SystemConfiguration.CaptiveNetwork

public class FGTDeviceSecController {
    public static let `default` = FGTDeviceSecController()
    private init() {}
}
extension FGTDeviceSecController: FGTDeviceSecControllerProtocol {
    public func getCurrentDevice() -> CurrentDevice {
        let model              =   UIDevice.modelName
        let uuid               =   getUUID()
        let imei               =   ""
        let platForm           =   UIDevice.current.systemName
        let platFormVersion    =   UIDevice.current.systemVersion
        let serialNumber       =   ""
        let manufacture        =   "Apple"
        let virtual            =   "\(simulator())"
        let rooted             =   "\(jailBroken())"
        let category           =   UIDevice.current.model
        
        let currentDevice  = CurrentDevice(model: model, uuid: uuid, imei: imei, platForm: platForm, platFormVersion: platFormVersion, serialNumber: serialNumber, manufacture: manufacture, virtual: virtual, rooted: rooted, category: category)
        
        return currentDevice
    }
    
    public func jailBroken() -> Bool {
        // Check 1 : existence of files that are common for jailbroken devices
        if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
            || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
            || FileManager.default.fileExists(atPath: "/bin/bash")
            || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
            || FileManager.default.fileExists(atPath: "/etc/apt")
            || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
            || FileManager.default.fileExists(atPath: "/usr/bin/ssh")
            || UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!) {
            
            //Device is jailbroken
            return true
        }
        
        // Check 2 : existence of root hiders
        if !JB().isSecurityCheckPassed() {
            //Device is jailbroken
            return true
        }
        
        // Check 3 : Reading and writing in system directories (sandbox violation)
        let stringToWrite = "Jailbreak Test"
        do
        {
            try stringToWrite.write(toFile:"/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
            
            //Device is jailbroken
            return true
            
        } catch {
            return false
        }
    }
    
    public func simulator() -> Bool {
        return TARGET_IPHONE_SIMULATOR == 1
    }
    
    public func invalidSource() -> Bool {
        if JB().isFromAppStore() {
            return false
        }
        return true
    }
    
    public func debugged() -> Bool {
        if JB().isDebugged() {
            return true
        }
        return false
    }
    
    private func getUUID() -> String {
        var deviceUUID: String? = KeychainWrapper.standard.string(forKey: "KeychainItem_UUID") ?? ""
        
        if (deviceUUID?.isEmpty) ?? false {
            KeychainWrapper.standard.set(UIDevice.current.identifierForVendor?.uuidString ?? "", forKey: "KeychainItem_UUID")
            deviceUUID = KeychainWrapper.standard.string(forKey: "KeychainItem_UUID")
        }
        return deviceUUID!
    }
    
    private func validDevice() -> Bool {
        let device = DCDevice.current
        // check if the device supports the DeviceCheck API
        guard device.isSupported else {
            return false
        }
        device.generateToken(completionHandler: { data, error in
            //            // send the base64 encoded device token string to your server
            //            if let token = data?.base64EncodedString() {
            //                if token == "" {
            //                    return false
            //                }
            //            } else {
            //                return false
            //            }
        })
        return true
    }
    
    public func isSecuredWIFIConnection() -> Bool {
        if let interfaceNames = CNCopySupportedInterfaces() as? [String] {
            if let firstInterface = interfaceNames.first {
                if let networkInfo = CNCopyCurrentNetworkInfo(firstInterface as CFString) as NSDictionary? {
                    if let securityType = networkInfo[kCNNetworkInfoKeySSIDData] as? [AnyHashable: Any] {
                        // Get the security type
                        if let securityTypeString = securityType["SECURITY"] as? String {
                            // Check if it's "None" for unencrypted or other values for encrypted networks
                            if securityTypeString.lowercased() == "none" {
                                // The Wi-Fi network is unencrypted
                                return false
                            } else {
                                // The Wi-Fi network is encrypted
                                return true
                            }
                        }
                    }
                }
            }
        }
        return true
    }
}

/*
 if let ssid = networkInfo[kCNNetworkInfoKeySSID] as? String {
 // Get the SSID (Wi-Fi network name)
 }
 */
