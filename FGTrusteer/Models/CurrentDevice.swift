//
//  CurrentDevice.swift
//  FGTrusteer
//
//  Created by Lahiru Chathuranga on 2023-09-07.
//

import Foundation

public struct CurrentDevice {
    
  public var model               :   String
  public var uuid                :   String
  public var imei                :   String
  public var platForm            :   String
  public var platFormVersion     :   String
  public var serialNumber        :   String
  public var manufacture         :   String
  public var virtual             :   String
  public var rooted              :   String
  public var category            :   String
    
    
    public init() {
        self.model              =   ""
        self.uuid               =   ""
        self.imei               =   ""
        self.platForm           =   ""
        self.platFormVersion    =   ""
        self.serialNumber       =   ""
        self.manufacture        =   ""
        self.virtual            =   ""
        self.rooted             =   ""
        self.category           =   ""
    }
    
    
    public init(model              :   String,
         uuid               :   String,
         imei               :   String,
         platForm           :   String,
         platFormVersion    :   String,
         serialNumber       :   String,
         manufacture        :   String,
         virtual            :   String,
         rooted             :   String,
         category           :   String
        ) {
        
        self.model              =   model
        self.uuid               =   uuid
        self.imei               =   imei
        self.platForm           =   platForm
        self.platFormVersion    =   platFormVersion
        self.serialNumber       =   serialNumber
        self.manufacture        =   manufacture
        self.virtual            =   virtual
        self.rooted             =   rooted
        self.category           =   category
    }
    
}
